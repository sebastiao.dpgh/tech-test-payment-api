using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Models;
using TechTestPaymentApi.Context;

namespace TrilhaApiDesafio.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int IdVenda)
        {
            var vendas = _context.Venda.Find(IdVenda);
            if (vendas == null)
                return NotFound();

            return Ok(vendas);
        }

        [HttpGet("ObterTodasVendas")]
        public IActionResult ObterTodos()
        {
            var vendas = _context.Venda.ToList();

            return Ok(vendas);
        }

        [HttpGet("ObterPorData")]
        public IActionResult ObterPorData(DateTime DataVenda)
        {
            var vendas = _context.Venda.Where(x => x.DataVenda.Date == DataVenda.Date);
            return Ok(vendas);
        }

        [HttpPost("(RegistrarVenda)")]
        public IActionResult Criar(Vendas venda)
        {
            if (venda.DataVenda == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da tarefa não pode ser vazia" });

            _context.Venda.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.IdVenda }, venda);
        }

        [HttpPut("{AlterarStatusVenda}")]
        public IActionResult Atualizar(int IdVenda, Vendas statusVenda)
        {
            var vendaBanco = _context.Venda.Find(IdVenda);

            if (vendaBanco == null)
                return NotFound();

            _context.Venda.Update(vendaBanco);
            _context.SaveChanges();
            return Ok();
        }
    }
}
