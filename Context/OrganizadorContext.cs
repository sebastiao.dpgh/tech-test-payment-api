using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Context
{
    public class OrganizadorContext : DbContext
    {
        public OrganizadorContext(DbContextOptions<OrganizadorContext> options) : base(options)
        {
            
        }

        public DbSet<Vendas> Venda { get; set; }
    }
}