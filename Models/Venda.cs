using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentApi.Models
{
    public class Vendas
    {
        public int IdVenda { get; set; }

        public DateTime DataVenda { get; set; }
        public string ItemVendido1 { get; set; }
        public string ItemVendido2 { get; set; }     

        private string _name;
        public string StatusVenda { get => _name; set => _name = "Aguardando pagamento"; }
    }
    
    public class Vendedor{
        public int IdVendedor { get; set; }

        public string NomeVendedor { get; set; }

        public int CpfVendedor { get; set; }
        
        public string EmailVendedor { get; set; }
    }
}